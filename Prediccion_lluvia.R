#Predicción de lluvia

# input: n -> Dias
#        w -> Ventana
rain <- function(n, w) {
  x <- round(runif(n, min = 0, max = 1),0)
  y <- vector(length = n) #Vector de Acumulados
  sol <- NULL
  
  #Acum
  y[1] <- x[1]
  
  for (i in 2:n) {
    y[i] <- y[i - 1] + x[i]
  }
  
  sol <- c(sol, y[w])
  
  #window
  for (i in (w + 1):n) {
    sol <- c(sol, (y[i] - y[i - w]));
  }
  
  return(sol)
  #print(x)
  #print(y)
  #print(sol)
}




#encontrar los valores 2, 3 de forma consecutiva en el vector y salirse
vec <- c(1, 2, 1, 2, 1, 3, 2, 2, 3, 2, 1, 2)
#proyecto hasta el final: construir un algoritmo que sea capaz de resolver sopas de letras

flag <- FALSE
i <- 1

while (i < length(vec)) {
  if (vec[i] == 2 & vec[i + 1] == 3) {
    flag <- TRUE
    print(i)
  }
  i <- i + 1
}

if (flag) {
  print("Si")
} else {
  print("No")
}
3+3 # Alejo
.3/3

# vector

datos<-matrix(nrow = 996, ncol = 3)

for (i in 5:1000) {
  datos[i-4, 1] <-i
  datos[i-4, 2] <-sum(round(runif(i)))
  datos[i-4, 3] <-datos[i-4, 2]/i
}

print(datos)
plot(datos[,3], type = "l", col="red")